#include <Servo.h>

#define BLYNK_PRINT Serial

#include <ESP8266_Lib.h>
#include <BlynkSimpleShieldEsp8266.h>
#include <EEPROM.h>

#include "Wire.h"
#include "SPI.h"  
#include "RTClib.h"

#define X_STEP_PIN         54
#define X_DIR_PIN          55
#define X_ENABLE_PIN       38
#define X_MIN_PIN           3
#define X_MAX_PIN           2

#define Y_STEP_PIN         60
#define Y_DIR_PIN          61
#define Y_ENABLE_PIN       56
#define Y_MIN_PIN          14
#define Y_MAX_PIN          15

#define Z_STEP_PIN         46
#define Z_DIR_PIN          48
#define Z_ENABLE_PIN       62
#define Z_MIN_PIN          18
#define Z_MAX_PIN          19

#define E_STEP_PIN         26
#define E_DIR_PIN          28
#define E_ENABLE_PIN       24

#define Q_STEP_PIN         36
#define Q_DIR_PIN          34
#define Q_ENABLE_PIN       30

#define SDPOWER            -1
#define SDSS               53

#define LED_PIN            13
#define FAN_PIN            9

#define PS_ON_PIN          12
#define KILL_PIN           -1

#define HEATER_0_PIN       10
#define HEATER_1_PIN       8

#define TEMP_0_PIN          13  
#define TEMP_1_PIN          14
   
#define WATER_PUMP 32
#define AIR_PUMP 47
#define FERTI_PUMP 45
#define BUZZER_PIN 25

#define x 5000                
#define stepsPerRevolution 200        

Servo myservo;

 int pos = 90;

 int timefer = 10000;

char auth[] = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"; 
//char auth[] = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";

char ssid[] = "cncfarm";
char pass[] = "12345678";

//
//char ssid[] = "gummybea";
//char pass[] = "30113011";

//char ssid[] = "botless";
//char pass[] = "741074107410";

bool seeded[24];
bool startedBlynk = false;
int timewaterall=1000;

int moisture_point[6];

#define EspSerial Serial2
#define ESP8266_BAUD 115200

ESP8266 wifi(&EspSerial);

float posX = 0;
float posY = 0;
float posZ = 0;

int x_min_stat,y_min_stat,z_min_stat;
int sensorValue;

int interval = 3000; 
unsigned long previousMillis;

int ledStatus = LOW;

int seed1,seed2,seed3;
int sensorPin = A3;
RTC_DS3231 RTC;
int tempC;
int Xmax,Xmin,Ymax,Ymin,Zmax,Zmin;

int sumMoisture;

bool ferimode=false;


int seedId = 1;
int plantNum = 1;

int fertiSet = 0;

int fertiStat[24];

int plantLED[24];
int timerwater = 0;

WidgetLED led1(102);
WidgetLED led2(103);
WidgetLED led3(104);
WidgetLED led4(105);
WidgetLED led5(106);
WidgetLED led6(107);
WidgetLED led7(108);
WidgetLED led8(109);
WidgetLED led9(110);
WidgetLED led10(111);
WidgetLED led11(112);
WidgetLED led12(113);
WidgetLED led13(114);
WidgetLED led14(115);
WidgetLED led15(116);
WidgetLED led16(117);
WidgetLED led17(118);
WidgetLED led18(119);
WidgetLED led19(120);
WidgetLED led20(121);
WidgetLED led21(122);
WidgetLED led22(123);
WidgetLED led23(124);
WidgetLED led24(125);


WidgetLCD lcd(V99);



void servoLeft(){
  
  myservo.write(180);

}

void servoRight(){

  myservo.write(0);
}

void servoCenter(){

  myservo.write(90);
}


void setup() {
  Wire.begin();
  RTC.begin();
  myservo.attach(5);
  delay(600)
  myservo.write(90);
  
  pinMode(FAN_PIN , OUTPUT);
  pinMode(HEATER_0_PIN , OUTPUT);
  pinMode(HEATER_1_PIN , OUTPUT);
  pinMode(LED_PIN  , OUTPUT);

  pinMode(X_STEP_PIN  , OUTPUT);
  pinMode(X_DIR_PIN    , OUTPUT);
  pinMode(X_ENABLE_PIN    , OUTPUT);

  pinMode(Y_STEP_PIN  , OUTPUT);
  pinMode(Y_DIR_PIN    , OUTPUT);
  pinMode(Y_ENABLE_PIN    , OUTPUT);

  pinMode(Z_STEP_PIN  , OUTPUT);
  pinMode(Z_DIR_PIN    , OUTPUT);
  pinMode(Z_ENABLE_PIN    , OUTPUT);

  pinMode(WATER_PUMP , OUTPUT);
  pinMode(AIR_PUMP  , OUTPUT);
  pinMode(FERTI_PUMP  , OUTPUT);

  pinMode(BUZZER_PIN,OUTPUT);
  
  digitalWrite(X_ENABLE_PIN    , LOW);
  digitalWrite(Y_ENABLE_PIN    , LOW);
  digitalWrite(Z_ENABLE_PIN    , LOW);

  pinMode(X_MIN_PIN,INPUT);
  pinMode(X_MAX_PIN,INPUT);

  pinMode(Y_MIN_PIN,INPUT);
  pinMode(Y_MAX_PIN,INPUT);

  pinMode(Z_MIN_PIN,INPUT);
  pinMode(Z_MAX_PIN,INPUT);

   digitalWrite(BUZZER_PIN , HIGH);
  digitalWrite(WATER_PUMP , HIGH);
  digitalWrite(AIR_PUMP, HIGH);
  digitalWrite(FERTI_PUMP, HIGH);

  

  delay(800);


  

  
  Serial.begin(250000);
  EspSerial.begin(ESP8266_BAUD);
  delay(10);
  

  Blynk.begin(auth,wifi,ssid, pass);
    //Blynk.begin(auth,wifi,ssid, pass, "192.168.137.1", 8080);

    

  digitalWrite(BUZZER_PIN,LOW);
  delay(100);
  digitalWrite(BUZZER_PIN,HIGH);
  delay(100);
  digitalWrite(BUZZER_PIN,LOW);
  delay(100);
  digitalWrite(BUZZER_PIN,HIGH);

  delay(60);
  
  if ( EEPROM.read ( 0 ) != 0xff )
    for (int i = 0; i < 25; ++i )
        plantLED [ i ] = EEPROM.read ( i );
  
}



BLYNK_CONNECTED() {
    Blynk.syncAll();
}


BLYNK_WRITE(V100)
{     
  seedId = param.asInt();

  
  
   
}

BLYNK_WRITE(V101)
{     
  plantNum = param.asInt();
   
}


BLYNK_WRITE(V127){
 int pindata = param.asInt();
  
  if (pindata) { 

         deletePoint(plantNum);

       
    }   

  
  
  }


BLYNK_WRITE(V1)
{   int pindata = param.asInt();
  
  if (pindata) { 

         plantProcess(seedId,plantNum);

       
    }    
    
}
    
BLYNK_WRITE(V2)
{   int pindata = param.asInt();
  
  if (pindata) {      
       //seedtask2 = 1;
       
    } 
    
}
BLYNK_WRITE(V3)
{   int pindata = param.asInt();
  
  if (pindata) {      
      // seedtask3 = 1;
       
    } 
 
}



////////////////////////////// PLANT /////////////////////////////

int plt1,plt2,plt3,plt4,plt5,plt6,plt7,plt8,plt9,plt10,plt11,plt12,plt13,plt14,plt15,plt16,plt17,plt18,plt19,plt20,plt21,plt22,plt23,plt24;

BLYNK_WRITE(V8)
{   int pindata = param.asInt();
  
  if (pindata) {   
seeded[0] = true;
       
plt1 = 1;

    } else {
       seeded[0] = false;
       //
    }
}
BLYNK_WRITE(V9)
{   int pindata = param.asInt();
  
  if (pindata) {   
   seeded[1] = true;
   
plt2 = 1;

    } else {

      seeded[1] = false;
       //
    }
}
BLYNK_WRITE(V10)
{   int pindata = param.asInt();
  
  if (pindata) {   

   seeded[2] = true;
   
plt3 = 1;

    } else {
      seeded[2] = false;
       
    }
}
BLYNK_WRITE(V11)
{   int pindata = param.asInt();
  
  if (pindata) {   
       seeded[3] = true;
       
plt4 = 1;

    } else {
      seeded[3] =false;
       //
    }
}
BLYNK_WRITE(V12)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
plt5 = 1;
    } else {
       //
    }
}
BLYNK_WRITE(V13)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
plt6 = 1;

    } else {
       //
    }
}
BLYNK_WRITE(V14)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
plt7 = 1;

    } else {
       //
    }
}
BLYNK_WRITE(V15)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
plt8 = 1;
    } else {
       //
    }
}
BLYNK_WRITE(V16)
{   int pindata = param.asInt();
  
  if (pindata) {   


plt9 = 1;

    } else {
       //
    }
}
BLYNK_WRITE(V17)
{   int pindata = param.asInt();
  
  if (pindata) {   
plt10 = 1;
    } else {
       //
    }
}
BLYNK_WRITE(V18)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
plt11 = 1;
    } else {
       //
    }
}
BLYNK_WRITE(V19)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
plt12= 1;
    } else {
       //
    }
}
BLYNK_WRITE(V20)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
plt13 = 1;
    } else {
       //
    }
}
BLYNK_WRITE(V21)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
plt14 = 1;
    } else {
       //
    }
}
BLYNK_WRITE(V22)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
plt15 = 1;
    } else {
       //
    }
}
BLYNK_WRITE(V23)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
plt16 = 1;
    } else {
       //
    }
}
BLYNK_WRITE(V24)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
plt17 = 1;

    } else {
       //
    }
}
BLYNK_WRITE(V25)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
plt18 = 1;
    } else {
       //
    }
}
BLYNK_WRITE(V26)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
plt19 = 1;
    } else {
       //
    }
}
BLYNK_WRITE(V27)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
plt20 = 1;
 //
    } else {
       //
    }
}
BLYNK_WRITE(V28)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
plt21 = 1;
 //
    } else {
       //
    }
}
BLYNK_WRITE(V29)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
plt22 = 1;
 //
    } else {
       //
    }
}
BLYNK_WRITE(V30)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
 plt23 = 1;
 //
    } else {
       //
    }
}
BLYNK_WRITE(V31)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
plt24 = 1;
 //
    } else {
       //
    }
}




///////////////
//////////////////////// TIMER ////////////////

BLYNK_WRITE(V32)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
 timerwater = 1;
 //
    } else {
       //
    }
}

BLYNK_WRITE(V33)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
 timerwater = 1;
 //
    } else {
       //
    }
}

BLYNK_WRITE(V34)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
 timerwater = 1;
 //
    } else {
      // 
    }
}


BLYNK_WRITE(V35)
{   int pindata = param.asInt();

if (pindata) {   
      
timerwater = 1;

 } else {

 }
}

////////////////////////////////////////

/////////////////////WATER


BLYNK_WRITE(V53)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
  timerwater = 1;
 //
    } else {
      // 
    }
}

//////FERTI///////////////////////////////

int ferti;
BLYNK_WRITE(V54)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
fertiProcess(plantNum);

    } else {
      
    }
}

//////////////////////////MOVE///////////
int moveXup,moveXdown,moveYup,moveYdown,moveZup,moveZdown;

BLYNK_WRITE(V36)
{   int pindata = param.asInt();
  
  if (pindata) {   
  moveYup = 1;
  delay(5);
  //
    } else {
     moveYup = 0;
    }
}

BLYNK_WRITE(V40)
{   int pindata = param.asInt();
  
  if (pindata) {   

  moveYdown = 1;
  delay(5);
  //
    } else {
      moveYdown = 0;
    }
}

BLYNK_WRITE(V41)
{   int pindata = param.asInt();
  
  if (pindata) {   

  moveXdown = 1;
  delay(5);
  //
    } else {
      moveXdown = 0;
    }
}

BLYNK_WRITE(V42)
{   int pindata = param.asInt();
  
  if (pindata) {   

  moveXup = 1;
  delay(5);
  //
    } else {
       moveXup = 0;
    }
}

BLYNK_WRITE(V37)
{   int pindata = param.asInt();
  
  if (pindata) {   

  moveZup = 1;
delay(5);
  //
    } else {
      moveZup = 0;
    }
}


BLYNK_WRITE(V38)
{   int pindata = param.asInt();
  
  if (pindata) {   

  moveZdown = 1;
  delay(5);
  //
    } else {
    moveZdown = 0;
    }
}


///////////////////////move to zero////////////
int movezero;
BLYNK_WRITE(V44)
{   int pindata = param.asInt();
  
  if (pindata) {  
     
  movezero = 1;
  
  
    } else {

       
    }
}

int movehome;
BLYNK_WRITE(V46)
{   int pindata = param.asInt();
  
  if (pindata) {  
     
  movehome = 1;
  
  
    } else {

     
    }
}





///////////////////////weed
int weedstart = 0;
BLYNK_WRITE(V43)
{   int pindata = param.asInt();
  
  if (pindata) {   
  weedstart = 1;
  //
  
    } else {
       //
    }
}


void handleBynkConnection(){
  bool result = Blynk.connect(900);
  if(result){
     Blynk.run(); 
  } else{
      Blynk.disconnect();
  }
}



void loop ()
{
  
  Blynk.run();

 

  if(startedBlynk == false){
    lcd.clear();
    lcd.print(1,0,"STARTING");
    startsettingzero();
    takemehome();
    startedBlynk = true;
  digitalWrite(BUZZER_PIN,LOW);
  delay(300);
  digitalWrite(BUZZER_PIN,HIGH);
    }

  
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval)
  {

    
    previousMillis = currentMillis;
    tempC = RTC.getTemperature();

  int sensorValue;
  sensorValue = analogRead(sensorPin);
  sensorValue = map(sensorValue, 0, 1023, 100, 0);
    
    Serial.println("R00");
     Serial.print("X position : ");
      Serial.println(posX);
      Serial.print("Y position : ");
      Serial.println(posY);
      Serial.print("Z position : ");
      Serial.println(posZ);
      Serial.print("temp : ");
      Serial.println(tempC);
      Serial.print("C ");
      Serial.print("Moisture  ");
      Serial.println(sensorValue);
      int pinsent=47;

      checkStatLEDPlant();


      
//      for(int i=0; i<6;i++){
//        
//        Blynk.virtualWrite(pinsent,moisture_point[i]);
//        pinsent++;
//        
//        }

  digitalWrite(LED_PIN,HIGH);
//
//  Blynk.virtualWrite(5,posX);
//
//  Blynk.virtualWrite(6,posY);
//
//  Blynk.virtualWrite(7,posZ);
//
Blynk.virtualWrite(39,tempC);
//
//  Blynk.virtualWrite(45,sensorValue);



   
  }

  if(weedstart ==1){
    weed();
    weedstart = 0;
    }

  

     if(movezero == 1){
       moveToPos(posX,posY,2);  
       myservo.write(90);;   
       moveToPos(0,0,2);  
       moveToPos(0,0,0);  
       movezero = 0;
      
      }

      if(plt1 == 1){
        plant1();
        
        }

    if(plt2 == 1){
        plant2();
        
        }

       if(plt3 == 1){
        plant3();
        
        }
        
        if(plt4 == 1){
        plant4();
        
        }
        

      if(plt5 == 1){
        plant5();
        
        }
        

       if(plt6 == 1){
        plant6();
        
        }
        

        if(plt7 == 1){
          plant7();
          }

        if(plt8 == 1){
          plant8();
          }

        if(plt9 == 1){
          plant9();
          }

          if(plt10 == 1){
          plant10();
          }
          if(plt11 == 1){
          plant11();
          }
          if(plt12 == 1){
          plant12();
          }
          if(plt13 == 1){
          plant13();
          }
          if(plt14 == 1){
          plant14();
          }
          if(plt15 == 1){
          plant15();
          }
          if(plt16 == 1){
          plant16();
          }
          if(plt17 == 1){
          plant17();
          }
          if(plt18 == 1){
          plant18();
          }
          if(plt19 == 1){
          plant19();
          }
          if(plt20 == 1){
          plant20();
          }
          if(plt21 == 1){
          plant21();
          }
          if(plt22 == 1){
          plant22();
          }
          if(plt23 == 1){
          plant23();
          }
          if(plt24 == 1){
          plant24();
          }

          if(movehome == 1){
            
            takemehome();
            
            movehome = 0;
            }

            if(timerwater == 1){
              
              checkhud();
              feedwaterall();
              takemehome();

              timerwater=0;
              
              }




//                if(moveXup == 1);
//                {
//                  moveXtomax();
//
//                  moveXup = 0;
//                  }
//
//                  if(moveXdown == 1);
//                {
//                  moveXtomin();
//                  moveXdown = 0;
//                  
//                  }
//
//                  
//                  if(moveYup == 1);
//                {
//                  moveYtomax();
//                  moveYup = 0;
//                  }
//                  if(moveYdown == 1);
//                {
//                  moveYtomin();
//                  moveYdown = 0;
//                  }
//
//                  if(moveZup == 1);
//                {
//                  moveZtomax();
//                  moveZup = 0;
//                  }
//                  if(moveZdown == 1);
//                {
//                  moveZtomin();
//                  moveZdown = 0;
//                  }
          

    digitalWrite(LED_PIN,LOW);
    


}

void startsettingzero(){

  
  Xmin = digitalRead(X_MIN_PIN);

  Ymin = digitalRead(Y_MIN_PIN);

  Zmin = digitalRead(Z_MIN_PIN);
  Zmax = digitalRead(Z_MAX_PIN);

while(Zmax!= 0&&Zmin!= 0){
  moveZtomax();
   Zmax = digitalRead(Z_MAX_PIN);
  }


  

  delay(200);

  

  
  while(Xmin!= 0){
    
    moveXtomin();
    Xmin = digitalRead(X_MIN_PIN);
    }

    delay(200);
  while(Ymin!= 0){
    moveYtomin();
    Ymin = digitalRead(Y_MIN_PIN);
    }
    
    delay(200);

  while(Zmin!= 0){
  moveZtomin();
   Zmin = digitalRead(Z_MIN_PIN);
  }

  delay(200);


posX= 0;
posY = 0;
posZ = 0;

  }


void moveToPos(float toposX,float toposY,float toposZ)
{
 
  float nowposX = posX;
  float stepposX;

  if(toposX > nowposX)
  {
    while(posX<toposX){
      moveXtomax();
      Blynk.run();
      
    }   
    Serial.write("OK\n");   
  }
  else if(toposX < nowposX)
        {
         while(posX> toposX){
            moveXtomin(); 
             Blynk.run(); 
            
         }
         Serial.write("OK\n");
          
        }

   
   
 


  float nowposY = posY;
  float stepposY;

  if(toposY > nowposY)
  {
    
    while(posY<toposY){
      moveYtomax();
       Blynk.run();
      
  
 }
    }  

  
  else if(toposY < nowposY)
        {
         
         while(posY>toposY){
            moveYtomin();  
             Blynk.run();
      
  
  
 } 
 }
         

        

 float nowposZ = posZ;
 float stepposZ;

  if(toposZ > nowposZ)
  {
    digitalWrite(Z_DIR_PIN    , LOW);
    while(posZ<toposZ){
      moveZtomax();
       Blynk.run();
      
    }  
    
  }
  else if(toposZ < nowposZ)
        {
         digitalWrite(Z_DIR_PIN    , HIGH);
         while(posZ>toposZ){
            moveZtomin(); 
             Blynk.run(); 
           
         }
           
        }
 

}
///////////////////////////   X  Move ///////////////////////////////////////////////////////////

void moveXtomax()
{

   Xmax = digitalRead(X_MAX_PIN);
  if(Xmax == 0){
      //idlemotor();
    }
    else{

  
  int totalRounds = 1;
  int dir =0;
  for (int rounds =0 ; rounds < 2*totalRounds; rounds++) {
       if (dir==0){ // set direction
         digitalWrite(X_DIR_PIN, LOW);
       } else {
         digitalWrite(X_DIR_PIN, HIGH);
       }
       delay(1); // give motors some breathing time
       //dir = 1-dir; // reverse direction
       for (int i=0; i < 1425; i++) {
         int t = abs(3200-i) / 200;
         digitalWrite(X_STEP_PIN, HIGH);
         delayMicroseconds(70 + t);
         digitalWrite(X_STEP_PIN, LOW);
         delayMicroseconds(70 + t);

       }
      Blynk.run();
       
    }
  posX=posX+1;

    }

  

}

void moveXtomin()
{
   Xmin = digitalRead(X_MIN_PIN);
  if(Xmin == 0){
      //idlemotor();
      posX=0;
    }else{
  int totalRounds = 1;
  int dir =1;
  for (int rounds =0 ; rounds < 2*totalRounds; rounds++) {
       if (dir==0){ // set direction
         digitalWrite(X_DIR_PIN, LOW);
       } else {
         digitalWrite(X_DIR_PIN, HIGH);
       }
       delay(1); // give motors some breathing time
       //dir = 1-dir; // reverse direction
       for (int i=0; i < 1425; i++) {
         int t = abs(3200-i) / 200;
         digitalWrite(X_STEP_PIN, HIGH);
         delayMicroseconds(70 + t);
         digitalWrite(X_STEP_PIN, LOW);
         delayMicroseconds(70 + t);
       
       }
        Blynk.run();
       
    }
   posX=posX-1; 

    }

   

}



////////////////////////////////////////////////////////////////

///////////////////////////   Y  Move ///////////////////////////////////////////////////////////

void moveYtomax()
{
   Ymax = digitalRead(Y_MAX_PIN);
  if(Ymax == 0){
      //idlemotor();
      
    }else{
  int totalRounds = 1;
  int dir =1;
  for (int rounds =0 ; rounds < 2*totalRounds; rounds++) {
       if (dir==0){ // set direction
         digitalWrite(Y_DIR_PIN, LOW);
       } else {
         digitalWrite(Y_DIR_PIN, HIGH);
       }
       delay(1); // give motors some breathing time
       //dir = 1-dir; // reverse direction
       for (int i=0; i < 1675; i++) {
         int t = abs(3200-i) / 200;
         digitalWrite(Y_STEP_PIN, HIGH);
         delayMicroseconds(70 + t);
         digitalWrite(Y_STEP_PIN, LOW);
         delayMicroseconds(70 + t);
        
       }
        Blynk.run();
    }
  posY=posY+1;

    }

}

void moveYtomin()
{

  Ymin = digitalRead(Y_MIN_PIN);
  if(Ymin == 0){
      
      
      posY = 0;
          
    }else{

int totalRounds = 1;
  int dir =0;
  for (int rounds =0 ; rounds < 2*totalRounds; rounds++) {
       if (dir==0){ // set direction
         digitalWrite(Y_DIR_PIN, LOW);
       } else {
         digitalWrite(Y_DIR_PIN, HIGH);
       }
       delay(1); // give motors some breathing time
       //dir = 1-dir; // reverse direction
       for (int i=0; i < 1675
       ; i++) {
         int t = abs(3200-i) / 200;
         digitalWrite(Y_STEP_PIN, HIGH);
         delayMicroseconds(70 + t);
         digitalWrite(Y_STEP_PIN, LOW);
         delayMicroseconds(70 + t);
       
       }
       Blynk.run();
      
    } 
   posY=posY-1; 

    }

}


////////////////////////////////////////////////////////////////

///////////////////////////   Z  Move //////////////////////////

void moveZtomax()
{
  Zmax = digitalRead(Z_MAX_PIN);
  if(Zmax == 0){
      //idlemotor();
      
    }else{
int totalRounds = 1;
  int dir =0;
  for (int rounds =0 ; rounds < 2*totalRounds; rounds++) {
       if (dir==0){ // set direction
         digitalWrite(Z_DIR_PIN, LOW);
       } else {
         digitalWrite(Z_DIR_PIN, HIGH);
       }
       delay(1); // give motors some breathing time
       //dir = 1-dir; // reverse direction
       for (int i=0; i < 6400; i++) {
         int t = abs(3200-i) / 200;
         digitalWrite(Z_STEP_PIN, HIGH);
         delayMicroseconds(7 + t);
         digitalWrite(Z_STEP_PIN, LOW);
         delayMicroseconds(7 + t);
         
       }
        Blynk.run();
     
    } 
  posZ=posZ+0.1;

    }

}

void moveZtomin()
{

  Zmin = digitalRead(Z_MIN_PIN);
  if(Zmin == 0){
      //idlemotor();
      
    }else{
  int totalRounds = 1;
  int dir =1;
  for (int rounds =0 ; rounds < 2*totalRounds; rounds++) {
       if (dir==0){ // set direction
         digitalWrite(Z_DIR_PIN, LOW);
       } else {
         digitalWrite(Z_DIR_PIN, HIGH);
       }
       delay(1); // give motors some breathing time
       //dir = 1-dir; // reverse direction
       for (int i=0; i < 6400; i++) {
         int t = abs(3200-i) / 200;
         digitalWrite(Z_STEP_PIN, HIGH);
         delayMicroseconds(7 + t);
         digitalWrite(Z_STEP_PIN, LOW);
         delayMicroseconds(7 + t);
         
       }
       Blynk.run();
      
    }
   posZ=posZ-0.1; 
    }

}


////////////////////////////////////////////////////////////////



void feedwaterall(){

moveToPos(posX,posY,2);


  int TheX = 1;
  int TheY = 2;

  for(int i=0;i<24;i++){
    delay(400);
    moveToPos(posX,posY,2);
     delay(400);
    moveToPos(TheX,TheY,2);
    
    if(plantLED[i] == true){
      delay(400);
      Serial.println("Seeded ");
      Serial.print(i);
      Serial.print(seeded[i]);
      digitalWrite(WATER_PUMP,LOW);
      delay(timewaterall);
      Blynk.run();
      delay(400);
      digitalWrite(WATER_PUMP,HIGH);
      delay(400);
      }

    TheX = TheX + 7;
    if(TheX>22){
      TheX = 1;
      TheY = TheY+7;
      }
    
    
    }

  }

 
 void checkhud(){
  
  
  myservo.write(0);;

  int TheX = 5;
  int TheY = 5;

  for(int i=0;i<6;i++){
    moveToPos(posX,posY,2);
    
    moveToPos(TheX,TheY,0);
    
  int sensorValue;
  sensorValue = analogRead(sensorPin);
  sensorValue = map(sensorValue, 0, 1023, 100, 0);
    
    delay(2000);
    Serial.println(sensorValue);
    moisture_point[i]=sensorValue;
    Serial.println("hud");
    Serial.print(i);
    Serial.print(" ");
    Serial.print(moisture_point[i]);
    
    if(i == 0){
    Blynk.virtualWrite(V47,moisture_point[i]);
    }
    else  if(i == 1){
    Blynk.virtualWrite(V48,moisture_point[i]);
    }else  if(i == 2){
    Blynk.virtualWrite(V49,moisture_point[i]);
    }else  if(i == 3){
    Blynk.virtualWrite(V50,moisture_point[i]);
    }else  if(i == 4){
    Blynk.virtualWrite(V51,moisture_point[i]);
    }else  if(i == 5){
    Blynk.virtualWrite(V52,moisture_point[i]);
    }
    
    
    TheX = TheX + 14;
    if(TheX>19){
      TheX = 5;
       if(TheY>20){
      TheY= TheY+14;
      }else {
      TheY = TheY+15;
      }
      }

      }

      sumMoisture = moisture_point[0] + moisture_point[1]+ moisture_point[2]+ moisture_point[3]+ moisture_point[4]+ moisture_point[5];
      
      delay(400);
      
      sumMoisture = sumMoisture / 6;



      delay(1000);
      Blynk.virtualWrite(V36,sumMoisture);
      delay(400);

      if(sumMoisture < 60){

        
        timewaterall = 10000;
        }
        else if(sumMoisture >= 60 && sumMoisture <= 80){
  
        timewaterall = 15000;
        }
        else if(sumMoisture > 80){
          
          timewaterall = 2;
          
          }

          moveToPos(posX,posY,2);
          delay(400);

myservo.write(90);;

    }

  
  

  void plant1(){


     moveToPos(1,2,posZ);

     if(fertiSet == 1) {
      moveToPos(posX,posY,2);
      delay(400);
      digitalWrite(FERTI_PUMP,LOW);
      delay(timefer);
      digitalWrite(FERTI_PUMP,HIGH);
      delay(400);
      }else{    
        
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
     delay(400);
     myservo.write(90);;
      }

    takemehome();
    
     
    plt1 = 0 ;

    
    
    }

      void plant2(){
        
     moveToPos(8,2,posZ);
         if(fertiSet == 1) {
      moveToPos(posX,posY,2);
      delay(400);
      digitalWrite(FERTI_PUMP,LOW);
      delay(timefer);
      digitalWrite(FERTI_PUMP,HIGH);
      delay(400);
      }else{ 
  
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
     delay(400);
     myservo.write(90);;
      }

    takemehome();
    
     
    plt2 = 0 ;
    
    }
      void plant3(){

     moveToPos(15,2,posZ);
    
         if(fertiSet == 1) {
      moveToPos(posX,posY,2);
      delay(400);
      digitalWrite(FERTI_PUMP,LOW);
      delay(timefer);
      digitalWrite(FERTI_PUMP,HIGH);
      delay(400);
      }else{ 
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
     delay(400);
     myservo.write(90);;
      }


    takemehome();
    
     
    plt3 = 0 ;
   
    
    }
      void plant4(){

     moveToPos(22,2,posZ);
        if(fertiSet == 1) {
      moveToPos(posX,posY,2);
      delay(400);
      digitalWrite(FERTI_PUMP,LOW);
      delay(timefer);
      digitalWrite(FERTI_PUMP,HIGH);
      delay(400);
      }else{ 
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
     delay(400);
     myservo.write(90);;
      }


    takemehome();
    
    plt4 = 0 ;

    }
      void plant5(){

     moveToPos(1,9,posZ);
        if(fertiSet == 1) {
      moveToPos(posX,posY,2);
      delay(400);
      digitalWrite(FERTI_PUMP,LOW);
      delay(timefer);
      digitalWrite(FERTI_PUMP,HIGH);
      delay(400);
      }else{ 
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
     delay(400);
     myservo.write(90);;
      }


    takemehome();
    
    plt5 = 0 ;
    
    }
      void plant6(){

     moveToPos(8,9,posZ);
          if(fertiSet == 1) {
      moveToPos(posX,posY,2);
      delay(400);
      digitalWrite(FERTI_PUMP,LOW);
      delay(timefer);
      digitalWrite(FERTI_PUMP,HIGH);
      delay(400);
      }else{ 
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
     delay(400);
     myservo.write(90);;
      }


    takemehome();
    
    
    }
      void plant7(){

     moveToPos(15,9,posZ);
       if(fertiSet == 1) {
      moveToPos(posX,posY,2);
      delay(400);
      digitalWrite(FERTI_PUMP,LOW);
      delay(timefer);
      digitalWrite(FERTI_PUMP,HIGH);
      delay(400);
      }else{ 
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
     delay(400);
     myservo.write(90);;
      }


    takemehome();  
    
    }
      void plant8(){

     moveToPos(22,9,posZ);
         if(fertiSet == 1) {
      moveToPos(posX,posY,2);
      delay(400);
      digitalWrite(FERTI_PUMP,LOW);
      delay(timefer);
      digitalWrite(FERTI_PUMP,HIGH);
      delay(400);
      }else{ 
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
     delay(400);
     myservo.write(90);;
      }
     takemehome();  
     plt8 = 0 ;

    
    }
      void plant9(){

     moveToPos(1,16,posZ);
         if(fertiSet == 1) {
      moveToPos(posX,posY,2);
      delay(400);
      digitalWrite(FERTI_PUMP,LOW);
      delay(timefer);
      digitalWrite(FERTI_PUMP,HIGH);
      delay(400);
      }else{ 
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
     delay(400);
     myservo.write(90);;
      }
     takemehome();  
     plt9 = 0 ;

    
    }
      void plant10(){

     moveToPos(8,16,posZ);
         if(fertiSet == 1) {
      moveToPos(posX,posY,2);
      delay(400);
      digitalWrite(FERTI_PUMP,LOW);
      delay(timefer);
      digitalWrite(FERTI_PUMP,HIGH);
      delay(400);
      }else{ 
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
     delay(400);
     myservo.write(90);;
      }
     takemehome();  
     plt10 = 0 ;

    
    }
      void plant11(){

     moveToPos(15,16,posZ);
         if(fertiSet == 1) {
      moveToPos(posX,posY,2);
      delay(400);
      digitalWrite(FERTI_PUMP,LOW);
      delay(timefer);
      digitalWrite(FERTI_PUMP,HIGH);
      delay(400);
      }else{ 
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
     delay(400);
     myservo.write(90);;
      }
     takemehome();  
     plt11 = 0 ;
    
    //
    }
      void plant12(){

     moveToPos(22,16,posZ);
         if(fertiSet == 1) {
      moveToPos(posX,posY,2);
      delay(400);
      digitalWrite(FERTI_PUMP,LOW);
      delay(timefer);
      digitalWrite(FERTI_PUMP,HIGH);
      delay(400);
      }else{ 
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
     delay(400);
     myservo.write(90);;
      }
     takemehome();  
     plt12 = 0 ;

    }
      void plant13(){

     moveToPos(1,23,posZ);
         if(fertiSet == 1) {
      moveToPos(posX,posY,2);
      delay(400);
      digitalWrite(FERTI_PUMP,LOW);
      delay(timefer);
      digitalWrite(FERTI_PUMP,HIGH);
      delay(400);
      }else{ 
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
     delay(400);
     myservo.write(90);;
      }
     takemehome();  
     plt13 = 0 ;


    }
      void plant14(){

     moveToPos(8,23,posZ);
         if(fertiSet == 1) {
      moveToPos(posX,posY,2);
      delay(400);
      digitalWrite(FERTI_PUMP,LOW);
      delay(timefer);
      digitalWrite(FERTI_PUMP,HIGH);
      delay(400);
      }else{ 
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
     delay(400);
     myservo.write(90);;
      }
     takemehome();  
     plt14 = 0 ;
    

    }
      void plant15(){
        
     moveToPos(15,23,posZ);
         if(fertiSet == 1) {
      moveToPos(posX,posY,2);
      delay(400);
      digitalWrite(FERTI_PUMP,LOW);
      delay(timefer);
      digitalWrite(FERTI_PUMP,HIGH);
      delay(400);
      }else{ 
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
     delay(400);
     myservo.write(90);;
      }
     takemehome();  
     plt15 = 0 ;
    
    }
      void plant16(){

     moveToPos(22,23,posZ);

         if(fertiSet == 1) {
      moveToPos(posX,posY,2);
      delay(400);
      digitalWrite(FERTI_PUMP,LOW);
      delay(timefer);
      digitalWrite(FERTI_PUMP,HIGH);
      delay(400);
      }else{ 
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
     delay(400);
     myservo.write(90);;
      }
     takemehome();  
     plt16 = 0 ;


    }
      void plant17(){
        
     moveToPos(1,30,posZ);
         if(fertiSet == 1) {
      moveToPos(posX,posY,2);
      delay(400);
      digitalWrite(FERTI_PUMP,LOW);
      delay(timefer);
      digitalWrite(FERTI_PUMP,HIGH);
      delay(400);
      }else{ 
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
     delay(400);
     myservo.write(90);;
      }
     takemehome();  
     plt17 = 0 ;
  
    }
      void plant18(){

     moveToPos(8,30,posZ);
         if(fertiSet == 1) {
      moveToPos(posX,posY,2);
      delay(400);
      digitalWrite(FERTI_PUMP,LOW);
      delay(timefer);
      digitalWrite(FERTI_PUMP,HIGH);
      delay(400);
      }else{ 
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
     delay(400);
     myservo.write(90);;
      }
     takemehome();  
     plt18 = 0 ;
    
    
    }
    
      void plant19(){

     moveToPos(15,30,posZ);
         if(fertiSet == 1) {
      moveToPos(posX,posY,2);
      delay(400);
      digitalWrite(FERTI_PUMP,LOW);
      delay(timefer);
      digitalWrite(FERTI_PUMP,HIGH);
      delay(400);
      }else{ 
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
     delay(400);
     myservo.write(90);;
      }
     takemehome();  
     plt19 = 0 ;
    
    }
    
      void plant20(){

     moveToPos(22,30,posZ);
         if(fertiSet == 1) {
      moveToPos(posX,posY,2);
      delay(400);
      digitalWrite(FERTI_PUMP,LOW);
      delay(timefer);
      digitalWrite(FERTI_PUMP,HIGH);
      delay(400);
      }else{ 
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
     delay(400);
     myservo.write(90);;
      }
     takemehome();  
     plt20 = 0 ;
    
    
    }
    
      void plant21(){

     moveToPos(1,37,posZ);
         if(fertiSet == 1) {
      moveToPos(posX,posY,2);
      delay(400);
      digitalWrite(FERTI_PUMP,LOW);
      delay(timefer);
      digitalWrite(FERTI_PUMP,HIGH);
      delay(400);
      }else{ 
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
     delay(400);
     myservo.write(90);;
      }
     takemehome();  
    plt21 = 0 ;


    }
    
      void plant22(){

     moveToPos(8,37,posZ);

         if(fertiSet == 1) {
      moveToPos(posX,posY,2);
      delay(400);
      digitalWrite(FERTI_PUMP,LOW);
      delay(timefer);
      digitalWrite(FERTI_PUMP,HIGH);
      delay(400);
      }else{ 
      
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
     delay(400);
     myservo.write(90);;
      }
     takemehome();  
     plt22 = 0 ;
  
    }
    
      void plant23(){

     moveToPos(15,37,posZ);
         if(fertiSet == 1) {
      moveToPos(posX,posY,2);
      delay(400);
      digitalWrite(FERTI_PUMP,LOW);
      delay(timefer);
      digitalWrite(FERTI_PUMP,HIGH);
      delay(400);
      }else{ 
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
     delay(400);
     myservo.write(90);;
      }
     takemehome();  
     plt23 = 0 ;
     
    }
    
      void plant24(){

     moveToPos(22,37,posZ);
         if(fertiSet == 1) {
      moveToPos(posX,posY,2);
      delay(400);
      digitalWrite(FERTI_PUMP,LOW);
      delay(timefer);
      digitalWrite(FERTI_PUMP,HIGH);
      delay(400);
      }else{ 
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
     delay(400);
     myservo.write(90);;
      }
     takemehome();  
     
    plt24 = 0 ;
    
    }

     void weed(){
      moveToPos(posX,posY,2);
      moveToPos(5,37,0);
      moveToPos(5,2,2);
      moveToPos(posX,posY,2);
      moveToPos(12,2,0);
      moveToPos(12,37,2);
      moveToPos(19,37,0);
      moveToPos(19,2,2);

      delay(500);
      takemehome();
    
      }

      void takemehome(){
        Blynk.run();
        moveToPos(posX,posY,2);
        Blynk.run();
        moveToPos(0,42,1);
        Blynk.run();
        
        }

       void plantProcess(int seedId,int plantNum){

        if(plantLED[plantNum-1] == false){

        if(seedId == 1){
          
       moveToPos(posX,posY,2);
       myservo.write(180);;
       moveToPos(9,42,0.2);
       digitalWrite(AIR_PUMP,LOW);
       delay(4000);
       moveToPos(posX,posY,2);

       
         }else if(seedId == 2){

          moveToPos(posX,posY,2);
       myservo.write(180);;
       moveToPos(12,42,0.5);
       digitalWrite(AIR_PUMP,LOW);
       delay(4000);
       moveToPos(posX,posY,2);

                 
          }else if(seedId == 3){
            
       moveToPos(posX,posY,2);
       myservo.write(180);;
       moveToPos(15,42,0.5);
       digitalWrite(AIR_PUMP,LOW);
       delay(4000);
       moveToPos(posX,posY,2);
       
       
      }
      else {
        while(true){
          
            digitalWrite(BUZZER_PIN,LOW);
            digitalWrite(LED_PIN,LOW);
            delay(50);
            digitalWrite(BUZZER_PIN,HIGH);
            digitalWrite(LED_PIN,LOW);
            delay(500);        
        }
        
        }

        digitalWrite(BUZZER_PIN,LOW);
        digitalWrite(LED_PIN,LOW);
        delay(100);
        digitalWrite(BUZZER_PIN,HIGH);
        digitalWrite(LED_PIN,LOW);
        delay(500);


        if(plantNum == 1){
          if(plantLED[0]== false){
            
          plant1();
          plantLED[0]=true;
          
          }else{lcd.print(3, 1, "Already Plant !" );}
           
          }

          else if(plantNum == 2){
          
          if(plantLED[1]== false){
          plant2();
          plantLED[1]=true;
          
          }else{lcd.print(3, 1, "Already Plant !" );}
          }

           else if(plantNum == 3){
          
          if(plantLED[2]== false){
          plant3();
          plantLED[2]=true;
          
          }else{lcd.print(3, 1, "Already Plant !" );}
          }

           else if(plantNum == 4){
          
          if(plantLED[3]== false){
          plant4();
          plantLED[3]=true;
          
          }else{lcd.print(3, 1, "Already Plant !" );}
          }

           else if(plantNum == 5){

          if(plantLED[4]== false){
          plant5();
          plantLED[4]=true;
          
          }else{lcd.print(3, 1, "Already Plant !" );}
          }

           else if(plantNum == 6){

          if(plantLED[5]== false){
          plant6();
          plantLED[5]=true;
          
          }else{lcd.print(3, 1, "Already Plant !" );}
          }

           else if(plantNum == 7){

          if(plantLED[6]== false){
          plant7();
          plantLED[6]=true;
          
          }else{lcd.print(3, 1, "Already Plant !" );}
          }

           else if(plantNum == 8){
          if(plantLED[7]== false){
          plant8();
          plantLED[7]=true;
          
          
          }else{lcd.print(3, 1, "Already Plant !" );}
          }

           else if(plantNum == 9){
          if(plantLED[8]== false){
          plant9();
          plantLED[8]=true;
          
          }else{lcd.print(3, 1, "Already Plant !" );}
          }

           else if(plantNum == 10){
          if(plantLED[9]== false){
          plant10();
          plantLED[9]=true;
          
          }else{lcd.print(3, 1, "Already Plant !" );}
          }

           else if(plantNum == 11){
          if(plantLED[10]== false){
          plant11();
          plantLED[10]=true;
          }else{lcd.print(3, 1, "Already Plant !" );}
          
          }

           else if(plantNum == 12){
          if(plantLED[11]== false){
          plant12();
          plantLED[11]=true;
          
          }else{lcd.print(3, 1, "Already Plant !" );}
          }

           else if(plantNum == 13){
          if(plantLED[12]== false){
          plant13();
          plantLED[12]=true;
          
          }else{lcd.print(3, 1, "Already Plant !" );}
          }

           else if(plantNum == 14){
          if(plantLED[13]== false){
          plant14();
          plantLED[13]=true;
          
          }else{lcd.print(3, 1, "Already Plant !" );}
          }

           else if(plantNum == 15){
          if(plantLED[14]== false){
          plant15();
          plantLED[14]=true;
          
          }else{lcd.print(3, 1, "Already Plant !" );}
          }

           else if(plantNum == 16){
          
          if(plantLED[15]== false){
          plant16();
          plantLED[15]=true;
          }else{lcd.print(3, 1, "Already Plant !" );}
          }

           else if(plantNum == 17){
          if(plantLED[16]== false){
          plant17();
          plantLED[16]=true;
          
          }else{lcd.print(3, 1, "Already Plant !" );}
          }

           else if(plantNum == 18){
          
          if(plantLED[17]== false){
          plant18();
          plantLED[17]=true;
          }else{lcd.print(3, 1, "Already Plant !" );}
          }

           else if(plantNum == 19){
          if(plantLED[18]== false){
          plant19();
          plantLED[18]=true;
          
          }else{lcd.print(3, 1, "Already Plant !" );}
          }

           else if(plantNum == 20){
          if(plantLED[19]== false){
          plant20();
          plantLED[19]=true;
          
          }else{lcd.print(3, 1, "Already Plant !" );}
          }

           else if(plantNum == 21){
          
          if(plantLED[20]== false){
          plant21();
          plantLED[20]=true;
          }else{lcd.print(3, 1, "Already Plant !" );}
          }

           else if(plantNum == 22){
          if(plantLED[21]== false){
          plant22();
          plantLED[21]=true;
          
          }else{lcd.print(3, 1, "Already Plant !" );}
          }

           else if(plantNum == 23){
          if(plantLED[22]== false){
          plant23();
          plantLED[22]=true;
          
          }else{lcd.print(3, 1, "Already Plant !" );}
          }

           else if(plantNum == 24){
          if(plantLED[23]== false){
          plant24();
          plantLED[23]=true;
          }else{lcd.print(3, 1, "Already Plant !" );}
          
          }
          else {
            delay(400);
            lcd.print(3, 1, "Already Plant !" );
            delay(800);
            }

        }

          delay(800);    
        
        checkStatLEDPlant();

        for ( int i = 0; i < 25; ++i )
   EEPROM.write ( i, plantLED [ i ] );
        
        }

        void checkStatLEDPlant(){

      
          for(int i=0;i<24;i++){

           if( plantLED[i] == true){
            
            if(i == 0){
              led1.on();
              }else if(i == 1){
                led2.on();
                }
                else if(i == 2){
                led3.on();
                }
                else if(i == 3){
                led4.on();
                }
                else if(i == 4){
                led5.on();
                }
                else if(i == 5){
                led6.on();
                }
                else if(i == 6){
                led7.on();
                }
                else if(i == 7){
                led8.on();
                }
                else if(i == 8){
                led9.on();
                }
                else if(i == 9){
                led10.on();
                }
                else if(i == 10){
                led11.on();
                }
                else if(i == 11){
                led12.on();
                }
                else if(i == 12){
                led13.on();
                }
                else if(i == 13){
                led14.on();
                }
                else if(i == 14){
                led15.on();
                }
                else if(i == 15){
                led16.on();
                }
                else if(i == 16){
                led17.on();
                }
                else if(i == 17){
                led18.on();
                }
                else if(i == 18){
                led19.on();
                }
                else if(i == 19){
                led20.on();
                }
                else if(i == 20){
                led21.on();
                }
                else if(i == 21){
                led22.on();
                }
                else if(i == 22){
                led23.on();
                }
                else if(i == 23){
                led24.on();
                }

                            
            }else if(plantLED[i] == false){
              
           
            if(i == 0){
              led1.off();
              }else if(i == 1){
                led2.off();
                }
                else if(i == 2){
                led3.off();
                }
                else if(i == 3){
                led4.off();
                }
                else if(i == 4){
                led5.off();
                }
                else if(i == 5){
                led6.off();
                }
                else if(i == 6){
                led7.off();
                }
                else if(i == 7){
                led8.off();
                }
                else if(i == 8){
                led9.off();
                }
                else if(i == 9){
                led10.off();
                }
                else if(i == 10){
                led11.off();
                }
                else if(i == 11){
                led12.off();
                }
                else if(i == 12){
                led13.off();
                }
                else if(i == 13){
                led14.off();
                }
                else if(i == 14){
                led15.off();
                }
                else if(i == 15){
                led16.off();
                }
                else if(i == 16){
                led17.off();
                }
                else if(i == 17){
                led18.off();
                }
                else if(i == 18){
                led19.off();
                }
                else if(i == 19){
                led20.off();
                }
                else if(i == 20){
                led21.off();
                }
                else if(i == 21){
                led22.off();
                }
                else if(i == 22){
                led23.off();
                }
                else if(i == 23){
                led24.off();
                }
              
              }
            
            delay(20);
            
            
            }

   delay(500);

   if(Blynk.connected()) {
    Blynk.syncAll();
}

delay(700);
  lcd.clear();
  if(plantLED[plantNum-1] == true){
    lcd.print(4,0,"PLANTED");
    lcd.print(1,0,plantNum);
    if(fertiStat[plantNum-1] == true){
    lcd.print(3, 1, "FERTILIZED" );
    }else{lcd.print(3, 1, "No FERTILIZER"); }
    }else if(plantLED[plantNum-1] == false){

    lcd.print(4,0,"No PLANT");
    lcd.print(1,0,plantNum);
    if(fertiStat[plantNum-1] == true){
    lcd.print(2, 1, "No FERTILIZER");
    }
    else{lcd.print(2, 1, "No FERTILIZER");}
    }



          
          
          
          }


  void fertiProcess(int plantNum){

    fertiSet = 1;

        digitalWrite(BUZZER_PIN,LOW);
        digitalWrite(LED_PIN,LOW);
        delay(100);
        digitalWrite(BUZZER_PIN,HIGH);
        digitalWrite(LED_PIN,LOW);
        delay(500);


        if(plantNum == 1){

          plant1();
          fertiStat[0]=true;
           
          }

          else if(plantNum == 2){
          
          
          plant2();
          fertiStat[1]=true;
          
          
          }

           else if(plantNum == 3){
          
          
          plant3();
          fertiStat[2]=true;
          
          
          }

           else if(plantNum == 4){
          
          
          plant4();
          fertiStat[3]=true;
          
          
          }

           else if(plantNum == 5){
          
          plant5();
          fertiStat[4]=true;
          
          
          }

           else if(plantNum == 6){
          
          plant6();
          fertiStat[5]=true;
          
          
          }

           else if(plantNum == 7){
          
          plant7();
          fertiStat[6]=true;
          
          
          }

           else if(plantNum == 8){
          plant8();
          fertiStat[7]=true;
          
          
          
          }

           else if(plantNum == 9){
          
          plant9();
          fertiStat[8]=true;
          
          
          }

           else if(plantNum == 10){
          
          plant10();
          fertiStat[9]=true;
          
          
          }

           else if(plantNum == 11){
          
          plant11();
          fertiStat[10]=true;
          
          
          }

           else if(plantNum == 12){
          
          plant12();
          fertiStat[11]=true;
          
          
          }

           else if(plantNum == 13){
          
          plant13();
          fertiStat[12]=true;
          
          
          }

           else if(plantNum == 14){
          
          plant14();
          fertiStat[13]=true;
          
          
          }

           else if(plantNum == 15){
          
          plant15();
          fertiStat[14]=true;
          
          
          }

           else if(plantNum == 16){
          
          
          plant16();
          fertiStat[15]=true;
          
          }

           else if(plantNum == 17){
          
          plant17();
          fertiStat[16]=true;
          
          
          }

           else if(plantNum == 18){
          
          
          plant18();
          fertiStat[17]=true;
          
          }

           else if(plantNum == 19){
          
          plant19();
          fertiStat[18]=true;
          
          
          }

           else if(plantNum == 20){
          
          plant20();
          fertiStat[19]=true;
          
          
          }

           else if(plantNum == 21){
          
          
          plant21();
          fertiStat[20]=true;
          
          }

           else if(plantNum == 22){
          
          plant22();
          fertiStat[21]=true;
          
          
          }

           else if(plantNum == 23){
          
          plant23();
          fertiStat[22]=true;
          
          
          }

           else if(plantNum == 24){
          
          plant24();
          fertiStat[23]=true;
          
          
          }

          delay(800);    
        
        checkStatLEDPlant();

        fertiSet = 0;
    
    
    
    
    
    
    }

    void deletePoint(int plantNum){

      plantLED[plantNum-1] = false;
      
      }

          

