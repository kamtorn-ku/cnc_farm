#include <Servo.h>

/* Comment this out to disable prints and save space */
#define BLYNK_PRINT DebugSerial


// You could use a spare Hardware Serial on boards that have it (like Mega)
#include <SoftwareSerial.h>
SoftwareSerial DebugSerial(2, 3); // RX, TX

#include <BlynkSimpleStream.h>

#include "Wire.h"
#include "SPI.h"  
#include "RTClib.h"

#define X_STEP_PIN         54
#define X_DIR_PIN          55
#define X_ENABLE_PIN       38
#define X_MIN_PIN           3
#define X_MAX_PIN           2

#define Y_STEP_PIN         60
#define Y_DIR_PIN          61
#define Y_ENABLE_PIN       56
#define Y_MIN_PIN          14
#define Y_MAX_PIN          15

#define Z_STEP_PIN         46
#define Z_DIR_PIN          48
#define Z_ENABLE_PIN       62
#define Z_MIN_PIN          18
#define Z_MAX_PIN          19

#define E_STEP_PIN         26
#define E_DIR_PIN          28
#define E_ENABLE_PIN       24

#define Q_STEP_PIN         36
#define Q_DIR_PIN          34
#define Q_ENABLE_PIN       30

#define SDPOWER            -1
#define SDSS               53

#define LED_PIN            13
#define FAN_PIN            9

#define PS_ON_PIN          12
#define KILL_PIN           -1

#define HEATER_0_PIN       10
#define HEATER_1_PIN       8

#define TEMP_0_PIN          13  
#define TEMP_1_PIN          14
   
#define WATER_PUMP    32
#define AIR_PUMP 47
#define FERTI_PUMP 45
#define BUZZER_PIN 49

#define x 5000                
#define stepsPerRevolution 200        

Servo myservo;

 int pos = 90;

//char auth[] = "880aed860a924d4f99284fae0d417b47"; 
char auth[] = "b3afbecac15b4339842b0effea7a0005";

bool seeded[24];
bool startedBlynk = false;
int timewaterall;

int moisture_point[6];


float posX = 0;
float posY = 0;
float posZ = 0;

int x_min_stat,y_min_stat,z_min_stat;
int sensorValue;

int interval = 3000; 
unsigned long previousMillis;

int ledStatus = LOW;

int seed1,seed2,seed3;
int sensorPin = A3;
RTC_DS3231 RTC;
int tempC;
int Xmax,Xmin,Ymax,Ymin,Zmax,Zmin;

int sumMoisture;

bool ferimode=false;
void servoLeft(){
  
  myservo.write(96);
  delay(1892);
  myservo.write(90);
}

void servoRight(){

  myservo.write(80);
  delay(1900);
  myservo.write(90);

  
}


void setup() {
  Wire.begin();
  RTC.begin();
  myservo.attach(23);
  myservo.write(90);
  
  pinMode(FAN_PIN , OUTPUT);
  pinMode(HEATER_0_PIN , OUTPUT);
  pinMode(HEATER_1_PIN , OUTPUT);
  pinMode(LED_PIN  , OUTPUT);

  pinMode(X_STEP_PIN  , OUTPUT);
  pinMode(X_DIR_PIN    , OUTPUT);
  pinMode(X_ENABLE_PIN    , OUTPUT);

  pinMode(Y_STEP_PIN  , OUTPUT);
  pinMode(Y_DIR_PIN    , OUTPUT);
  pinMode(Y_ENABLE_PIN    , OUTPUT);

  pinMode(Z_STEP_PIN  , OUTPUT);
  pinMode(Z_DIR_PIN    , OUTPUT);
  pinMode(Z_ENABLE_PIN    , OUTPUT);

  pinMode(WATER_PUMP , OUTPUT);
  pinMode(AIR_PUMP  , OUTPUT);
  pinMode(FERTI_PUMP  , OUTPUT);

  pinMode(BUZZER_PIN,OUTPUT);
  
  digitalWrite(X_ENABLE_PIN    , LOW);
  digitalWrite(Y_ENABLE_PIN    , LOW);
  digitalWrite(Z_ENABLE_PIN    , LOW);

  pinMode(X_MIN_PIN,INPUT);
  pinMode(X_MAX_PIN,INPUT);

  pinMode(Y_MIN_PIN,INPUT);
  pinMode(Y_MAX_PIN,INPUT);

  pinMode(Z_MIN_PIN,INPUT);
  pinMode(Z_MAX_PIN,INPUT);


  digitalWrite(WATER_PUMP , HIGH);
  digitalWrite(AIR_PUMP, HIGH);
  digitalWrite(FERTI_PUMP, HIGH);

  
  myservo.write(90);
  delay(800);


  


  delay(10);
  

  // Debug console
  DebugSerial.begin(9600);

  Serial.begin(9600);
  Blynk.begin(Serial, auth);

    digitalWrite(BUZZER_PIN,HIGH);
  delay(100);
  digitalWrite(BUZZER_PIN,LOW);
}

//BLYNK_WRITE(V0)
//{   int pindata = param.asInt();
//  
//  if (pindata) {      
//       pin_activated();   
//    } else {
//       pin_deactivated(); 
//    }
//}
//
//void pin_activated()
//{
//  Blynk.virtualWrite(V0, HIGH);
//  feedwaterall();
//  Blynk.virtualWrite(V0, LOW);
//  //
//  
//  }
//
//void pin_deactivated()
//{
//  //
//  }

int seedtask1,seedtask2,seedtask3;

/////////////////////// seed /////////////////////
BLYNK_WRITE(V1)
{   int pindata = param.asInt();
  
  if (pindata) { 

         seedtask1 =1;

       
    }    
    
}
    
BLYNK_WRITE(V2)
{   int pindata = param.asInt();
  
  if (pindata) {      
       seedtask2 = 1;
       
    } 
    
}
BLYNK_WRITE(V3)
{   int pindata = param.asInt();
  
  if (pindata) {      
       seedtask3 = 1;
       
    } 
 
}



////////////////////////////// PLANT /////////////////////////////

int plt1,plt2,plt3,plt4,plt5,plt6,plt7,plt8,plt9,plt10,plt11,plt12,plt13,plt14,plt15,plt16,plt17,plt18,plt19,plt20,plt21,plt22,plt23,plt24;

BLYNK_WRITE(V8)
{   int pindata = param.asInt();
  
  if (pindata) {   
seeded[0] = true;
       
plt1 = 1;

    } else {
       seeded[0] = false;
       //
    }
}
BLYNK_WRITE(V9)
{   int pindata = param.asInt();
  
  if (pindata) {   
   seeded[1] = true;
   
plt2 = 1;

    } else {

      seeded[1] = false;
       //
    }
}
BLYNK_WRITE(V10)
{   int pindata = param.asInt();
  
  if (pindata) {   

   seeded[2] = true;
   
plt3 = 1;

    } else {
      seeded[2] = false;
       
    }
}
BLYNK_WRITE(V11)
{   int pindata = param.asInt();
  
  if (pindata) {   
       seeded[3] = true;
       
plt4 = 1;

    } else {
      seeded[3] =false;
       //
    }
}
BLYNK_WRITE(V12)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
plt5 = 1;
    } else {
       //
    }
}
BLYNK_WRITE(V13)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
plt6 = 1;

    } else {
       //
    }
}
BLYNK_WRITE(V14)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
plt7 = 1;

    } else {
       //
    }
}
BLYNK_WRITE(V15)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
plt8 = 1;
    } else {
       //
    }
}
BLYNK_WRITE(V16)
{   int pindata = param.asInt();
  
  if (pindata) {   


plt9 = 1;

    } else {
       //
    }
}
BLYNK_WRITE(V17)
{   int pindata = param.asInt();
  
  if (pindata) {   
plt10 = 1;
    } else {
       //
    }
}
BLYNK_WRITE(V18)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
plt11 = 1;
    } else {
       //
    }
}
BLYNK_WRITE(V19)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
plt12= 1;
    } else {
       //
    }
}
BLYNK_WRITE(V20)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
plt13 = 1;
    } else {
       //
    }
}
BLYNK_WRITE(V21)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
plt14 = 1;
    } else {
       //
    }
}
BLYNK_WRITE(V22)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
plt15 = 1;
    } else {
       //
    }
}
BLYNK_WRITE(V23)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
plt16 = 1;
    } else {
       //
    }
}
BLYNK_WRITE(V24)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
plt17 = 1;

    } else {
       //
    }
}
BLYNK_WRITE(V25)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
plt18 = 1;
    } else {
       //
    }
}
BLYNK_WRITE(V26)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
plt19 = 1;
    } else {
       //
    }
}
BLYNK_WRITE(V27)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
plt20 = 1;
 //
    } else {
       //
    }
}
BLYNK_WRITE(V28)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
plt21 = 1;
 //
    } else {
       //
    }
}
BLYNK_WRITE(V29)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
plt22 = 1;
 //
    } else {
       //
    }
}
BLYNK_WRITE(V30)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
 plt23 = 1;
 //
    } else {
       //
    }
}
BLYNK_WRITE(V31)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
plt24 = 1;
 //
    } else {
       //
    }
}




///////////////
//////////////////////// TIMER ////////////////
int timerwater;
BLYNK_WRITE(V32)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
 timerwater = 1;
 //
    } else {
       //
    }
}

BLYNK_WRITE(V33)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
 timerwater = 1;
 //
    } else {
       //
    }
}

BLYNK_WRITE(V34)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
 timerwater = 1;
 //
    } else {
      // 
    }
}


BLYNK_WRITE(V35)
{   int pindata = param.asInt();

if (pindata) {   
      
timerwater = 1;

 } else {

 }
}

////////////////////////////////////////

/////////////////////WATER


BLYNK_WRITE(V53)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
  timerwater = 1;
 //
    } else {
      // 
    }
}

//////FERTI///////////////////////////////

int ferti;
BLYNK_WRITE(V54)
{   int pindata = param.asInt();
  
  if (pindata) {   
       
  ferimode = 1;
 //
    } else {
      ferimode = 0;
    }
}

//////////////////////////MOVE///////////
int moveXup,moveXdown,moveYup,moveYdown,moveZup,moveZdown;

BLYNK_WRITE(V36)
{   int pindata = param.asInt();
  
  if (pindata) {   
  moveYup = 1;
  delay(5);
  //
    } else {
     moveYup = 0;
    }
}

BLYNK_WRITE(V40)
{   int pindata = param.asInt();
  
  if (pindata) {   

  moveYdown = 1;
  delay(5);
  //
    } else {
      moveYdown = 0;
    }
}

BLYNK_WRITE(V41)
{   int pindata = param.asInt();
  
  if (pindata) {   

  moveXdown = 1;
  delay(5);
  //
    } else {
      moveXdown = 0;
    }
}

BLYNK_WRITE(V42)
{   int pindata = param.asInt();
  
  if (pindata) {   

  moveXup = 1;
  delay(5);
  //
    } else {
       moveXup = 0;
    }
}

BLYNK_WRITE(V37)
{   int pindata = param.asInt();
  
  if (pindata) {   

  moveZup = 1;
delay(5);
  //
    } else {
      moveZup = 0;
    }
}


BLYNK_WRITE(V38)
{   int pindata = param.asInt();
  
  if (pindata) {   

  moveZdown = 1;
  delay(5);
  //
    } else {
    moveZdown = 0;
    }
}


///////////////////////move to zero////////////
int movezero;
BLYNK_WRITE(V44)
{   int pindata = param.asInt();
  
  if (pindata) {  
     
  movezero = 1;
  
  
    } else {

       
    }
}

int movehome;
BLYNK_WRITE(V46)
{   int pindata = param.asInt();
  
  if (pindata) {  
     
  movehome = 1;
  
  
    } else {

     
    }
}





///////////////////////weed
int weedstart = 0;
BLYNK_WRITE(V43)
{   int pindata = param.asInt();
  
  if (pindata) {   
  weedstart = 1;
  //
  
    } else {
       //
    }
}


void handleBynkConnection(){
  bool result = Blynk.connect(12000);
  if(result){
     Blynk.run(); 
  } else{
      Blynk.disconnect();
  }
}



void loop ()
{

  Blynk.begin(Serial, auth);
  Blynk.run();

  myservo.write(90);

  if(startedBlynk == false){
    startsettingzero();
    startedBlynk = true;
    }

  
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval)
  {

    
    previousMillis = currentMillis;
    tempC = RTC.getTemperature();

  int sensorValue;
  sensorValue = analogRead(sensorPin);
  sensorValue = map(sensorValue, 0, 1023, 100, 0);
    
    Serial.println("R00");
     Serial.print("X position : ");
      Serial.println(posX);
      Serial.print("Y position : ");
      Serial.println(posY);
      Serial.print("Z position : ");
      Serial.println(posZ);
      Serial.print("temp : ");
      Serial.println(tempC);
      Serial.print("C ");
      Serial.print("Moisture  ");
      Serial.println(sensorValue);
      int pinsent=47;


      
      for(int i=0; i<6;i++){
        
        Blynk.virtualWrite(pinsent,moisture_point[i]);
        pinsent++;
        
        }

  digitalWrite(LED_PIN,HIGH);

  Blynk.virtualWrite(5,posX);

  Blynk.virtualWrite(6,posY);

  Blynk.virtualWrite(7,posZ);

  Blynk.virtualWrite(39,tempC);

  Blynk.virtualWrite(45,sensorValue);



   
  }

  if(weedstart ==1){
    weed();
    weedstart = 0;
    }

  if(seedtask1 == 1){
    
       moveToPos(posX,posY,2);
       servoLeft();
       moveToPos(9,42,0.2);
       digitalWrite(AIR_PUMP,HIGH);
       delay(4000);
       moveToPos(posX,posY,2);
       seedtask1= 0;
    
    }
   if(seedtask2 == 1){
    
       moveToPos(posX,posY,2);
       servoLeft();
       moveToPos(12,42,0.5);
       digitalWrite(AIR_PUMP,HIGH);
       delay(4000);
       moveToPos(posX,posY,2);
       seedtask2 = 0;
    
    }

    if(seedtask3 == 1){
      
       moveToPos(posX,posY,2);
       servoLeft();
       moveToPos(15,42,0.5);
       digitalWrite(AIR_PUMP,HIGH);
       delay(4000);
       moveToPos(posX,posY,2);
       seedtask3 = 0 ;
       
      }

     if(movezero == 1){
       moveToPos(posX,posY,2);  
       servoLeft();   
       moveToPos(0,0,2);  
       moveToPos(0,0,0);  
       movezero = 0;
      
      }

      if(plt1 == 1){
        plant1();
        
        }

    if(plt2 == 1){
        plant2();
        
        }

       if(plt3 == 1){
        plant3();
        
        }
        
        if(plt4 == 1){
        plant4();
        
        }
        

      if(plt5 == 1){
        plant5();
        
        }
        

       if(plt6 == 1){
        plant6();
        
        }
        

        if(plt7 == 1){
          plant7();
          }

        if(plt8 == 1){
          plant8();
          }

        if(plt9 == 1){
          plant9();
          }

          if(plt10 == 1){
          plant10();
          }
          if(plt11 == 1){
          plant11();
          }
          if(plt12 == 1){
          plant12();
          }
          if(plt13 == 1){
          plant13();
          }
          if(plt14 == 1){
          plant14();
          }
          if(plt15 == 1){
          plant15();
          }
          if(plt16 == 1){
          plant16();
          }
          if(plt17 == 1){
          plant17();
          }
          if(plt18 == 1){
          plant18();
          }
          if(plt19 == 1){
          plant19();
          }
          if(plt20 == 1){
          plant20();
          }
          if(plt21 == 1){
          plant21();
          }
          if(plt22 == 1){
          plant22();
          }
          if(plt23 == 1){
          plant23();
          }
          if(plt24 == 1){
          plant24();
          }

          if(movehome == 1){
            
            takemehome();
            
            movehome = 0;
            }

            if(timerwater == 1){
              
              checkhud();
              feedwaterall();
              takemehome();

              timerwater=0;
              
              }



//                if(moveXup == 1);
//                {
//                  moveXtomax();
//
//                  moveXup = 0;
//                  }
//
//                  if(moveXdown == 1);
//                {
//                  moveXtomin();
//                  moveXdown = 0;
//                  
//                  }
//
//                  
//                  if(moveYup == 1);
//                {
//                  moveYtomax();
//                  moveYup = 0;
//                  }
//                  if(moveYdown == 1);
//                {
//                  moveYtomin();
//                  moveYdown = 0;
//                  }
//
//                  if(moveZup == 1);
//                {
//                  moveZtomax();
//                  moveZup = 0;
//                  }
//                  if(moveZdown == 1);
//                {
//                  moveZtomin();
//                  moveZdown = 0;
//                  }
          

    digitalWrite(LED_PIN,LOW);
    


}

void startsettingzero(){

  
  Xmin = digitalRead(X_MIN_PIN);

  Ymin = digitalRead(Y_MIN_PIN);

  Zmin = digitalRead(Z_MIN_PIN);
  Zmax = digitalRead(Z_MAX_PIN);

while(Zmax!= 0&&Zmin!= 0){
  moveZtomax();
   Zmax = digitalRead(Z_MAX_PIN);
  }


  

  delay(200);

  

  
  while(Xmin!= 0){
    
    moveXtomin();
    Xmin = digitalRead(X_MIN_PIN);
    }

    delay(200);
  while(Ymin!= 0){
    moveYtomin();
    Ymin = digitalRead(Y_MIN_PIN);
    }
    
    delay(200);

  while(Zmin!= 0){
  moveZtomin();
   Zmin = digitalRead(Z_MIN_PIN);
  }

  delay(200);


posX= 0;
posY = 0;
posZ = 0;

  }


void moveToPos(float toposX,float toposY,float toposZ)
{
 
  float nowposX = posX;
  float stepposX;

  if(toposX > nowposX)
  {
    while(posX<toposX){
      moveXtomax();
      
    }   
    Serial.write("OK\n");   
  }
  else if(toposX < nowposX)
        {
         while(posX> toposX){
            moveXtomin();  
            
         }
         Serial.write("OK\n");
          
        }

   
   
 


  float nowposY = posY;
  float stepposY;

  if(toposY > nowposY)
  {
    
    while(posY<toposY){
      moveYtomax();
      
  
 }
    }  

  
  else if(toposY < nowposY)
        {
         
         while(posY>toposY){
            moveYtomin();  
      
  
  
 } 
 }
         

        

 float nowposZ = posZ;
 float stepposZ;

  if(toposZ > nowposZ)
  {
    digitalWrite(Z_DIR_PIN    , LOW);
    while(posZ<toposZ){
      moveZtomax();
      
    }  
    
  }
  else if(toposZ < nowposZ)
        {
         digitalWrite(Z_DIR_PIN    , HIGH);
         while(posZ>toposZ){
            moveZtomin();  
           
         }
           
        }
 

}
///////////////////////////   X  Move ///////////////////////////////////////////////////////////

void moveXtomax()
{

   Xmax = digitalRead(X_MAX_PIN);
  if(Xmax == 0){
      //idlemotor();
    }
    else{

  
  int totalRounds = 1;
  int dir =0;
  for (int rounds =0 ; rounds < 2*totalRounds; rounds++) {
       if (dir==0){ // set direction
         digitalWrite(X_DIR_PIN, LOW);
       } else {
         digitalWrite(X_DIR_PIN, HIGH);
       }
       delay(1); // give motors some breathing time
       //dir = 1-dir; // reverse direction
       for (int i=0; i < 1425; i++) {
         int t = abs(3200-i) / 200;
         digitalWrite(X_STEP_PIN, HIGH);
         delayMicroseconds(70 + t);
         digitalWrite(X_STEP_PIN, LOW);
         delayMicroseconds(70 + t);
         
         
       }
        Blynk.run();
    
       
    }
  posX=posX+1;

    }

  

}

void moveXtomin()
{
   Xmin = digitalRead(X_MIN_PIN);
  if(Xmin == 0){
      //idlemotor();
      posX=0;
    }else{
  int totalRounds = 1;
  int dir =1;
  for (int rounds =0 ; rounds < 2*totalRounds; rounds++) {
       if (dir==0){ // set direction
         digitalWrite(X_DIR_PIN, LOW);
       } else {
         digitalWrite(X_DIR_PIN, HIGH);
       }
       delay(1); // give motors some breathing time
       //dir = 1-dir; // reverse direction
       for (int i=0; i < 1425; i++) {
         int t = abs(3200-i) / 200;
         digitalWrite(X_STEP_PIN, HIGH);
         delayMicroseconds(70 + t);
         digitalWrite(X_STEP_PIN, LOW);
         delayMicroseconds(70 + t);
        
       }
        Blynk.run();
       
       
    }
   posX=posX-1; 

    }

   

}



////////////////////////////////////////////////////////////////

///////////////////////////   Y  Move ///////////////////////////////////////////////////////////

void moveYtomax()
{
   Ymax = digitalRead(Y_MAX_PIN);
  if(Ymax == 0){
      //idlemotor();
      
    }else{
  int totalRounds = 1;
  int dir =1;
  for (int rounds =0 ; rounds < 2*totalRounds; rounds++) {
       if (dir==0){ // set direction
         digitalWrite(Y_DIR_PIN, LOW);
       } else {
         digitalWrite(Y_DIR_PIN, HIGH);
       }
       delay(1); // give motors some breathing time
       //dir = 1-dir; // reverse direction
       for (int i=0; i < 1675; i++) {
         int t = abs(3200-i) / 200;
         digitalWrite(Y_STEP_PIN, HIGH);
         delayMicroseconds(70 + t);
         digitalWrite(Y_STEP_PIN, LOW);
         delayMicroseconds(70 + t);
         
       }
        Blynk.run();
       
    }
  posY=posY+1;

    }

}

void moveYtomin()
{

  Ymin = digitalRead(Y_MIN_PIN);
  if(Ymin == 0){
      
      
      posY = 0;
          
    }else{

int totalRounds = 1;
  int dir =0;
  for (int rounds =0 ; rounds < 2*totalRounds; rounds++) {
       if (dir==0){ // set direction
         digitalWrite(Y_DIR_PIN, LOW);
       } else {
         digitalWrite(Y_DIR_PIN, HIGH);
       }
       delay(1); // give motors some breathing time
       //dir = 1-dir; // reverse direction
       for (int i=0; i < 1675
       ; i++) {
         int t = abs(3200-i) / 200;
         digitalWrite(Y_STEP_PIN, HIGH);
         delayMicroseconds(70 + t);
         digitalWrite(Y_STEP_PIN, LOW);
         delayMicroseconds(70 + t);
        
       }
        Blynk.run();
      
      
    } 
   posY=posY-1; 

    }

}


////////////////////////////////////////////////////////////////

///////////////////////////   Z  Move //////////////////////////

void moveZtomax()
{
  Zmax = digitalRead(Z_MAX_PIN);
  if(Zmax == 0){
      //idlemotor();
      
    }else{
int totalRounds = 1;
  int dir =0;
  for (int rounds =0 ; rounds < 2*totalRounds; rounds++) {
       if (dir==0){ // set direction
         digitalWrite(Z_DIR_PIN, LOW);
       } else {
         digitalWrite(Z_DIR_PIN, HIGH);
       }
       delay(1); // give motors some breathing time
       //dir = 1-dir; // reverse direction
       for (int i=0; i < 6400; i++) {
         int t = abs(3200-i) / 200;
         digitalWrite(Z_STEP_PIN, HIGH);
         delayMicroseconds(3 + t);
         digitalWrite(Z_STEP_PIN, LOW);
         delayMicroseconds(3 + t);
         
       }
        Blynk.run();
       
     
    } 
  posZ=posZ+0.1;

    }

}

void moveZtomin()
{

  Zmin = digitalRead(Z_MIN_PIN);
  if(Zmin == 0){
      //idlemotor();
      
    }else{
  int totalRounds = 1;
  int dir =1;
  for (int rounds =0 ; rounds < 2*totalRounds; rounds++) {
       if (dir==0){ // set direction
         digitalWrite(Z_DIR_PIN, LOW);
       } else {
         digitalWrite(Z_DIR_PIN, HIGH);
       }
       delay(1); // give motors some breathing time
       //dir = 1-dir; // reverse direction
       for (int i=0; i < 6400; i++) {
         int t = abs(3200-i) / 200;
         digitalWrite(Z_STEP_PIN, HIGH);
         delayMicroseconds(3 + t);
         digitalWrite(Z_STEP_PIN, LOW);
         delayMicroseconds(3 + t);
         
       }
        Blynk.run();
      
      
    }
   posZ=posZ-0.1; 
    }

}


////////////////////////////////////////////////////////////////
void idlemotor(){
  digitalWrite(X_DIR_PIN    , LOW);
  digitalWrite(X_STEP_PIN    , LOW);

  digitalWrite(Y_DIR_PIN    , LOW);
  digitalWrite(Y_STEP_PIN    , LOW);


  digitalWrite(Z_DIR_PIN    , LOW);
  digitalWrite(Z_STEP_PIN    , LOW);
Blynk.run();

  }

void fertifeed(){

  moveToPos(posX,posY,2);


  int TheX = 1;
  int TheY = 2;

  for(int i=0;i<24;i++){
    
    moveToPos(TheX,TheY,2);
    if(seeded[i] == true){
      Serial.println("Seeded ");
      Serial.print(i);
      Serial.print(seeded[i]);
      digitalWrite(FERTI_PUMP,LOW);
      delay(5000);
      digitalWrite(FERTI_PUMP,HIGH);
      //
      }

    TheX = TheX + 7;
    if(TheX>22){
      TheX = 1;
      TheY = TheY+7;
      }
    
    
    }



    //
  
  
  }

void feedwaterall(){

moveToPos(posX,posY,2);


  int TheX = 1;
  int TheY = 2;

  for(int i=0;i<24;i++){
    
    moveToPos(TheX,TheY,2);
    if(seeded[i] == true){
      Serial.println("Seeded ");
      Serial.print(i);
      Serial.print(seeded[i]);
      digitalWrite(WATER_PUMP,LOW);
      delay(timewaterall);
      digitalWrite(WATER_PUMP,HIGH);
      //
      }

    TheX = TheX + 7;
    if(TheX>22){
      TheX = 1;
      TheY = TheY+7;
      }
    
    
    }

  }

 
 void checkhud(){
  
  
  servoRight();

  int TheX = 5;
  int TheY = 5;

  for(int i=0;i<6;i++){
    moveToPos(posX,posY,2);
    
    moveToPos(TheX,TheY,0);
    int sensorValue = analogRead(A0);
    sensorValue = map(sensorValue,1024,200,0,100);   
    delay(2000);
    Serial.println(sensorValue);
    moisture_point[i]=sensorValue;
    Serial.println("hud");
    Serial.print(i);
    Serial.print(" ");
    Serial.print(moisture_point[i]);

    sumMoisture = sumMoisture + moisture_point[i];
    TheX = TheX + 14;
    if(TheX>19){
      TheX = 5;
       if(TheY>20){
      TheY= TheY+14;
      }else {
      TheY = TheY+15;
      }
      }

      }

      sumMoisture = sumMoisture / 6;


      if(sumMoisture < 60){

        
        timewaterall = 10000;
        }
        else

        if(sumMoisture >= 60 && sumMoisture <= 80){

        
        timewaterall = 15000;
        }
        else if(sumMoisture > 80){
          
          timewaterall = 2;
          }

servoLeft();
    }

  
  

  void plant1(){


     moveToPos(1,2,posZ);
     
     if(ferimode == 1){
       moveToPos(posX,posY,2);
       digitalWrite(FERTI_PUMP , LOW);
       delay(5000);
       digitalWrite(FERTI_PUMP , HIGH);
      }else{
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
      }

    takemehome();
    servoRight();
     
    plt1 = 0 ;

    
    
    }

      void plant2(){
        
     moveToPos(8,2,posZ);
     
     if(ferimode == 1){
       moveToPos(posX,posY,2);
       digitalWrite(FERTI_PUMP , LOW);
       delay(3000);
       digitalWrite(FERTI_PUMP , HIGH);
      }else{
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
      }

    takemehome();
    servoRight();
     
    plt2 = 0 ;
    
    }
      void plant3(){

     moveToPos(15,2,posZ);
    
if(ferimode == 1){
       moveToPos(posX,posY,2);
       digitalWrite(FERTI_PUMP , LOW);
       delay(3000);
       digitalWrite(FERTI_PUMP , HIGH);
      }else{
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
      }


    takemehome();
    servoRight();
     
    plt3 = 0 ;
   
    
    }
      void plant4(){

     moveToPos(22,2,posZ);
     if(ferimode == 1){
       moveToPos(posX,posY,2);
       digitalWrite(FERTI_PUMP , LOW);
       delay(3000);
       digitalWrite(FERTI_PUMP , HIGH);
      }else{
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
      }


    takemehome();
    servoRight();
    plt4 = 0 ;

    }
      void plant5(){

     moveToPos(1,9,posZ);
    if(ferimode == 1){
       moveToPos(posX,posY,2);
       digitalWrite(FERTI_PUMP , LOW);
       delay(3000);
       digitalWrite(FERTI_PUMP , HIGH);
      }else{
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
      }


    takemehome();
    servoRight();
    plt5 = 0 ;
    
    }
      void plant6(){

     moveToPos(8,9,posZ);
     if(ferimode == 1){
       moveToPos(posX,posY,2);
       digitalWrite(FERTI_PUMP , LOW);
       delay(3000);
       digitalWrite(FERTI_PUMP , HIGH);
      }else{
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
      }


    takemehome();
    servoRight();
    
    }
      void plant7(){

     moveToPos(15,9,posZ);
    if(ferimode == 1){
       moveToPos(posX,posY,2);
       digitalWrite(FERTI_PUMP , LOW);
       delay(3000);
       digitalWrite(FERTI_PUMP , HIGH);
      }else{
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
      }


    takemehome();  servoRight();
    
    }
      void plant8(){

     moveToPos(22,9,posZ);
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);

     takemehome();  servoRight();
     plt8 = 0 ;

    
    }
      void plant9(){

     moveToPos(1,16,posZ);
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
     takemehome();  servoRight();
     plt9 = 0 ;

    
    }
      void plant10(){

     moveToPos(8,16,posZ);
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
     takemehome();  servoRight();
     plt10 = 0 ;

    
    }
      void plant11(){

        moveToPos(15,16,posZ);
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
     takemehome();  servoRight();
    plt11 = 0 ;
    
    //
    }
      void plant12(){

     moveToPos(22,16,posZ);
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
     takemehome();  servoRight();
     plt12 = 0 ;

    }
      void plant13(){

     moveToPos(1,23,posZ);
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
     takemehome();  servoRight();
     plt13 = 0 ;


    }
      void plant14(){

     moveToPos(8,23,posZ);
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
     takemehome();  servoRight();
     plt14 = 0 ;
    

    }
      void plant15(){
        
     moveToPos(15,23,posZ);
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
     takemehome();  servoRight();
     plt15 = 0 ;
    
    }
      void plant16(){

     moveToPos(22,23,posZ);
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
     takemehome();  servoRight();
     plt16 = 0 ;


    }
      void plant17(){
        
     moveToPos(1,30,posZ);
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
     takemehome();  servoRight();
     plt17 = 0 ;
  
    }
      void plant18(){

     moveToPos(8,30,posZ);
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
     takemehome();  servoRight();
     plt18 = 0 ;
    
    
    }
    
      void plant19(){

     moveToPos(15,30,posZ);
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
     takemehome();  servoRight();
     plt19 = 0 ;
    
    }
    
      void plant20(){

     moveToPos(22,30,posZ);
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
     takemehome();  servoRight();
     plt20 = 0 ;
    
    
    }
    
      void plant21(){

     moveToPos(1,37,posZ);
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
     takemehome();  servoRight();
    plt21 = 0 ;


    }
    
      void plant22(){

     moveToPos(8,37,posZ);
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
     takemehome();  servoRight();
     plt22 = 0 ;
  
    }
    
      void plant23(){

     moveToPos(15,37,posZ);
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
     takemehome();  servoRight();
     plt23 = 0 ;
     
    }
    
      void plant24(){

     moveToPos(22,37,posZ);
     moveToPos(posX,posY,0);
     delay(3000);
     digitalWrite(AIR_PUMP , HIGH);
     moveToPos(posX,posY,2);
     takemehome();  servoRight();
    plt24 = 0 ;
    
    }

     void weed(){
      moveToPos(posX,posY,2);
      moveToPos(5,37,0);
      moveToPos(5,2,2);
      moveToPos(posX,posY,2);
      moveToPos(12,2,0);
      moveToPos(12,37,2);
      moveToPos(19,37,0);
      moveToPos(19,2,2);
    
      }

      void takemehome(){
        
        moveToPos(posX,posY,2);
        moveToPos(0,42,1);
        
        
        }

